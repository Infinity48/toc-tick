''' Tock-Tic
	Copyright (C) 2021 Infinity48

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.
'''
import arcade, os

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_TITLE = 'Tock-Tic'
GRAVITY = 1.0
PLAYER_MOVEMENT_SPEED = 8
PLAYER_JUMP_SPEED = 20
SPRITE_SCALING = 1

window = arcade.Window(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
os.chdir('assets')
arcade.load_font('fonts/DejaVuSans.ttf')

class Ending(arcade.View):
	def on_draw(self):
		arcade.start_render()
		
		arcade.draw_lrwh_rectangle_textured(144, 72, 512, 512, arcade.load_texture('images/endface.png'))
	
	def on_key_press(self, key, key_modifiers):
		arcade.close_window()
	
	def on_mouse_press(self, x, y, delta_x, delta_y):
		arcade.close_window()
	
	def on_show_view(self):
		arcade.set_background_color(arcade.color.ORANGE)

class Game(arcade.View):
	def __init__(self):
		super().__init__()

		self.map_height = None
		self.level = 0
		self.timer = 0.0
		self.deaths = 0
		self.sounds = {'boing':arcade.Sound('sounds/juskiddink_boing.ogg'),'bgm':arcade.Sound('sounds/music.ogg', streaming=True),'die':arcade.Sound(':resources:sounds/fall3.wav')}
		# Preloading sounds as doing what I did previously no longer works with newer versions of Arcade.
		for i in self.sounds:
			_ = self.sounds[i].play(0)
			arcade.stop_sound(_)
		self.music = self.sounds['bgm'].play(loop=True)
		
		self.player = arcade.SpriteSolidColor(32, 32, arcade.color.BLUE)
		self.wall_list = None
		self.jumppad_list = None
		self.spike_list = None

	def setup(self, has_died=False):
		self.player.center_x = 32
		self.player.center_y = 64
		self.player.change_y = 0
		layer_options = {'Walls':{'use_spatial_hash': True}, 'Trampolines':{'use_spatial_hash': True}, 'Deadly':{'use_spatial_hash': True}}
		map = arcade.tilemap.load_tilemap(f'levels/{self.level}.json', SPRITE_SCALING, layer_options)
		self.wall_list = map.sprite_lists['Walls']
		self.jumppad_list = map.sprite_lists['Trampolines']
		self.spike_list = map.sprite_lists['Deadly']
		self.map_height = map.height * map.tile_height
		if has_died:
			self.sounds['die'].play()
			self.deaths += 1
			self.timer = 30.0
		else:
			self.timer += 30.0
		self.camera = arcade.Camera(SCREEN_WIDTH, SCREEN_HEIGHT)
		self.ui_camera = arcade.Camera(SCREEN_WIDTH, SCREEN_HEIGHT)
		self.player.physics_engine = arcade.PhysicsEnginePlatformer(self.player, self.wall_list, GRAVITY)

	def on_draw(self):
		arcade.start_render()
		self.camera.use()

		self.jumppad_list.draw()
		self.player.draw()
		self.wall_list.draw()
		self.spike_list.draw()
		
		self.ui_camera.use()
		arcade.draw_rectangle_filled(80, 20, 128, 20, arcade.color.BLACK)
		arcade.draw_lrwh_rectangle_textured(20, 15, 12, 12, arcade.load_texture('images/time.png'))
		arcade.draw_lrwh_rectangle_textured(90, 15, 12, 12, arcade.load_texture('images/death.png'))
		arcade.draw_text(str(round(self.timer, 2)), 35, 15, arcade.color.WHITE, 12, 32, 'left', 'DejaVu Sans')
		arcade.draw_text(str(self.deaths), 106, 15, arcade.color.WHITE, 12, 32, 'left', 'DejaVu Sans')

	def on_update(self, delta_time):
		self.player.physics_engine.update()
		for thing in self.spike_list:
			thing.update_animation()
		self.timer -= delta_time
		if self.player.center_y > self.map_height:
			if self.level >= 4:
				window.show_view(Ending())
			else:
				print('You win!')
				self.level += 1
				self.setup()
		elif self.player.center_y > SCREEN_HEIGHT / 2:
			self.camera.move_to((0, self.player.center_y - SCREEN_HEIGHT / 2))
		
		if arcade.check_for_collision_with_list(self.player, self.spike_list) or self.timer < 0 or self.player.center_y < 0:
			self.setup(True)

	def on_key_press(self, key, key_modifiers):
		"""
		Called whenever a key on the keyboard is pressed.

		For a full list of keys, see:
		https://arcade.academy/arcade.key.html
		"""
		if key == arcade.key.LEFT:
			self.player.change_x = -PLAYER_MOVEMENT_SPEED
		elif key == arcade.key.RIGHT:
			self.player.change_x = PLAYER_MOVEMENT_SPEED
		elif key == arcade.key.UP:
			if self.player.physics_engine.can_jump() and arcade.check_for_collision_with_list(self.player, self.jumppad_list):
				self.sounds['boing'].play(0.25)
				self.player.change_y = PLAYER_JUMP_SPEED
		elif key == arcade.key.R:
			self.setup(True)
	
	def on_key_release(self, key, key_modifiers):
		"""
		Called whenever the user lets off a previously pressed key.
		"""
		if key == arcade.key.LEFT or key == arcade.key.RIGHT:
			self.player.change_x = 0
	
	def on_show_view(self):
		arcade.set_background_color(arcade.color.DARK_IMPERIAL_BLUE)
		self.setup()
	
	def on_hide_view(self):
		arcade.stop_sound(self.music)

def main():
	""" Main method """
	window.show_view(Game())
	arcade.run()

if __name__ == "__main__":
	main()
